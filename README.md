<div align="center">

# CI-GENDOC

[![Latest Release](../-/jobs/artifacts/main/raw/public/badges/release.svg?job=badges)](https://gitlab.com/Jagostini/ci-gendoc/-/releases)
[![Total Releases](../-/jobs/artifacts/main/raw/public/badges/releases.svg?job=badges)](https://gitlab.com/Jagostini/ci-gendoc/-/releases)
[![Maintainer](https://img.shields.io/badge/maintained%20by-Jules%20Agostini-e00000?style=flat-square)](https://www.linkedin.com/in/julesagostini/)

</div>

## Table of Contents

- [CI-GENDOC](#ci-gendoc)
  - [Table of Contents](#table-of-contents)
  - [🌐 Que fait le projet ?](#-que-fait-le-projet-)
  - [🔰 Comment les utilisateurs peuvent-ils commencer à utiliser le projet ?](#-comment-les-utilisateurs-peuvent-ils-commencer-à-utiliser-le-projet-)
    - [⚠️ Pré-requis](#️-pré-requis)
    - [Dépendances](#dépendances)
    - [✏️ Variables](#️-variables)
    - [⚙️ Infos utiles](#️-infos-utiles)
  - [⚖ Licence, Guide de Contribution \& Code de Conduite](#-licence-guide-de-contribution--code-de-conduite)

## 🌐 Que fait le projet ?

<!-- ajouter description -->

**ci-gendoc** est un CLI permettant de générer une documentation markdown pour les fichiers `.gitlab-ci.yml`.
Actuellement, le résultat est la génération par défaut d'un fichier "README.md" dans le répertoire courant de votre execution.
Vous pouvez spécifier un emplacement (directory) dans lequel sera injecter notre documentation avec l'argument `-o, --output`
Vous pouvez fournir en entrée un fichier yaml spécifique avec l'argument `-i, --input`. L'objectif est de détecter automatiquement les fichiers contenant `.gitlab-ci.yml`.

## 🔰 Comment les utilisateurs peuvent-ils commencer à utiliser le projet ?

<!--Présenter comment se servir de l'application.-->

Vous pouvez consulter à tout moment l'aide avec `ci-gendoc --help`

```bash
Usage: ci-gendoc [OPTIONS] <COMMAND>

Commands:
  generate
          generation of documentation
  help
          Print this message or the help of the given subcommand(s)

Options:
  -i, --input <FILE>
          Sets a custom path to an input file
  -o, --output-dir <DIRECTORY>
          Sets a custom directory for the output file
  -v, --verbose...
          Verbose mode
  -h, --help
          Print help
  -V, --version
          Print version
```

### ⚠️ Pré-requis

- rustc 1.75.0 (82e1608df 2023-12-21)
- cargo 1.75.0 (1d8b05cdd 2023-11-20)

### Dépendances

Pour le moment j'utilise les dépendances suivantes :

```toml
[dependencies]
clap = { version = "4.4.7", features = ["derive"] }
serde = { version = "1.0.193", features = ["derive"] }
serde_yaml = "0.9.29"
```

> Pensez à le tester avant de mettre à jour la version.

### ✏️ Variables

Aucune variable supplémentaire à configurer pour faire fonctionner le projet.

### ⚙️ Infos utiles

<!--Décrire ici, tous les complements d'informations que vous pourriez apporter -->

La structure de mon projet va ressembler à ça :

```bash
ci-gendoc/
├── src/
│   ├── main.rs     // Fichier principal permettant de tout coordonner
│   ├── cli.rs      // Configuration du CLI
│   ├── yaml.rs     // Lire et parser le fichier YAML pour décrire ce qu'il fait
│   ├── markdown.rs // (option) Générer la documentation Markdown à partir de la structure de données issue de yaml.rs
│   └── template.rs // (option) Gestion d'un template de documentation pour laisser la liberté d'adapter suivant le contexte la forme souhaité
├── tests/
│   ├── integration_test.rs       // Test d'intégration de l'application
│   ├── unit_tests.rs             // Test unitaires de l'application
│   └── test_files/
│       └── sample_gitlab_ci.yml  // fichier d'exemple à utiliser pour tester l'application
├── Cargo.toml
```

## ⚖ Licence, Guide de Contribution & Code de Conduite

Le code source principale de ce projet est sous licence [![AGPL v3](https://img.shields.io/badge/License-AGPL_v3-blue.svg)](LICENCE.md).

Les oeuvres dérivés, documentations, annexes liées à ce projet sont sous licences [![CC BY-SA 4.0](https://img.shields.io/badge/License-CC_BY--SA_4.0-orange.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

Vous souhaitez y apporter des changements ou des améliorations ? Lisez notre [guide de contribution](CONTRIBUTING.md).

Les contributeurs souscrivent au code de conduite des contributeurs adapté du [![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.0-purple.svg)](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html). Lisez notre [guide de conduite](CODE_OF_CONDUCT.md).

Ce projet respect le [Semantic Versioning 2.0](https://semver.org/) et ce mécanisme est automatisé via le projet [![semantic-release: conventional-commit](https://img.shields.io/badge/semantic--release-enabled-darkblue?logo=semantic-release)](https://github.com/semantic-release/semantic-release).

Vous avez détecter une vulnérabilité dans l'application et souhaitez nous informer. Lisez notre [guide de sécurité](SECURITY.md).

Les badges d'en-tête sont maintenus avec le projet [Badges Gitlab](https://gitlab.com/felipe_public/badges-gitlab) [![Documentation Status](https://readthedocs.org/projects/badges-gitlab/badge/?version=latest)](https://badges-gitlab.readthedocs.io/en/latest/?badge=latest)

Pour toute suggestion d'amélioration ou consulter les modifications à venir consulter notre [issue tracker](https://gitlab.com/Jagostini/ci-gendoc/-/issues)
