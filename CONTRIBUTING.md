# CONTRIBUER

**Veuillez contribuer!**

Lorsque vous contribuez à ce dépôt, veuillez d'abord discuter de la modification que vous souhaitez apporter via issue,
email, demande de fusion ou toute autre méthode, avec les propriétaires de ce dépôt avant de faire un changement. Les problèmes et les demandes de fusion sont les bienvenus.

Veuillez noter que nous avons un [code de conduite](CODE_OF_CONDUCT.md), veuillez le respecter dans toutes vos interactions avec le projet.

## Processus de demande de fusion

1. Utiliser la branche **develop** comme cible
2. Mettre à jour/ajouter des tests si nécessaire
3. Mise à jour du fichier README.md si nécessaire
4. Mettre à jour toute documentation nécessaire (README.md, wiki, docs/*.md ...)
5. S'assurer que la CI passe
6. Assurez-vous que votre commit est formaté en utilisant [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-%23FE5196?logo=conventionalcommits&logoColor=white)](https://www.conventionalcommits.org/fr/v1.0.0/)
7. Si vous êtes un mainteneur, votre PR doit être fusionné par un autre mainteneur. Si vous n'êtes pas un mainteneur, demandez à l'un d'entre eux de fusionner votre PR (mentionnez-le dans votre PR en utilisant `@...`).

## Remerciements

**Merci** pour votre aide à l'amélioration de ce projet !
