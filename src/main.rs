// main.rs

mod cli;
mod yaml;

use std::path::PathBuf;

use clap::Parser;
use crate::cli::Cli;
// use crate::yaml::parse_yaml_file;

fn main() {
    let cli = Cli::parse();

    // Vérifier si l'argument --input est présent
    // Si c'est le cas, on affiche le chemin vers le fichier d'entrée
    if let Some(input_file_path) = cli.input.as_deref() {
        println!("The path to the input file is: {}", input_file_path.display());
    }

    // Vérifier si l'argument --output-dir est présent
    // Si c'est le cas on affiche le chemin vers le répertoire d'output
    if let Some(output_dir) = cli.output_dir.as_deref() {
        // Vérifier si le répertoire existe, sinon le créer
        if !output_dir.exists() {
            if let Err(err) = std::fs::create_dir_all(output_dir) {
                eprintln!("Error creating output directory: {}", err);
                std::process::exit(1);
            }
        }
        println!("The output directory is: {}", output_dir.display());
    }

    // Vérifier si l'argument --verbose est présent
    // Si c'est le cas, on affiche le verbose
    // Petit test pour préparer le debug mode
    match cli.verbose {
        0 => println!("Debug mode is off"),
        1 => println!("Debug mode is kind of on"),
        2 => println!("Debug mode is on"),
        3 => println!("Debug mode is really on"),
        _ => println!("Don't be crazy"),
    }

    // Vérifier si l'argument --input est présent
    // Si c'est le cas, on analyse le fichier YAML spécifié
    // if let Some(input_file_path) = cli.input.as_deref() {
    //     let input_file_path_buf = PathBuf::from(input_file_path);
    //     let yaml_data = parse_yaml_file(&input_file_path_buf);
    //     match yaml_data {
    //         Ok(data) => {
    //             let _ = main_inner(cli, data);
    //         }
    //         Err(err) => {
    //             eprintln!("Error parsing YAML: {}", err);
    //             std::process::exit(1);
    //         }
    //     }
    // }

    // On appelle la fonction main_inner du module cli
    // Cette fonction est appelée par le main
    match &cli.command {
        // Utilisation de la fonction main_inner du module cli
        cli::Commands::Generate(_generate_args) => {
            // Analyse du fichier YAML
            if let Some(input_file_path) = cli.input.as_deref() {
                let input_file_path_buf = PathBuf::from(input_file_path);
                let yaml_data = yaml::parse_yaml_file(&input_file_path_buf);
                match yaml_data {
                    Ok(data) => {
                        // Utilisation de la fonction main_inner du module cli avec les données YAML
                        if let Err(err) = cli::main_inner(cli, data) {
                            eprintln!("Error in main_inner: {}", err);
                            std::process::exit(1);
                        }
                    }
                    Err(err) => {
                        eprintln!("Error parsing YAML: {}", err);
                        std::process::exit(1);
                    }
                }
            } else {
                // Gestion du cas où --input n'est pas spécifié
                eprintln!("Error: --input must be specified for the Generate command");
                std::process::exit(1);
            }
        }
    }
}
