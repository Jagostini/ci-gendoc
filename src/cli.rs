// cli.rs


use clap::{Args, Parser, Subcommand};
use std::{env, fs::File, io::Write, path::PathBuf};

use crate::yaml::{YamlData, fill_markdown_content};

#[derive(Parser)]
#[command(version, author, about, long_about = None)]
#[command(next_line_help = true)]
pub struct Cli {
    #[arg(short, long, value_name = "FILE", help = "Sets a custom path to an input file")]
    pub input: Option<PathBuf>,

    #[arg(short, long, value_name = "DIRECTORY", help = "Sets a custom directory for the output file")]
    pub output_dir: Option<PathBuf>,

    #[arg(short, long, help = "Verbose mode", action = clap::ArgAction::Count)]
    pub verbose: u8,

    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    Generate(GenerateArgs),
}

#[derive(Args)]
pub struct GenerateArgs {
    pub name: Option<String>,
}

pub fn main_inner(cli: Cli, yaml_data: YamlData) -> Result<(), String> {
    match &cli.command {
        Commands::Generate(generate_args) => {
            // Utilisation du nom spécifié ou le nom par défaut "README"
            let file_name = generate_args.name.as_deref().unwrap_or("README");

            // Utilisation du répertoire spécifié ou le répertoire courant
            let output_dir = cli
                .output_dir
                .as_ref()
                .map_or_else(|| env::current_dir().unwrap(), |path| path.clone());

            // Création du chemin complet du fichier de sortie avec extension ".md"
            let output_file_path = output_dir.join(format!("{}.md", file_name));

            // Génération du contenu du fichier Markdown avec les données YAML
            let markdown_content = fill_markdown_content(&yaml_data);

            // Écriture du contenu dans le fichier
            let mut file =
                File::create(&output_file_path).map_err(|err| format!("Error: {}", err))?;
            file.write_all(markdown_content.as_bytes())
                .map_err(|err| format!("Error: {}", err))?;
            println!(
                "Markdown file '{}' created successfully.",
                output_file_path.display()
            );

            Ok(())
        }
    }
}
