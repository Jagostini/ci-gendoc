// yaml.rs

use serde::{Deserialize, Serialize};
use std::{fs::File, io::Read, path::Path};
use serde_yaml;

#[derive(Debug, Deserialize, Serialize)]
pub struct Job {
    pub stage: String,
    // Ajoutez d'autres champs en fonction de vos besoins
}

#[derive(Debug, Deserialize, Serialize)]
pub struct YamlData {
    pub jobs: Vec<Job>,
}

pub fn parse_yaml_file(file_path: &Path) -> Result<YamlData, String> {
    let mut file = File::open(file_path).map_err(|err| format!("Error opening file: {}", err))?;
    let mut content = String::new();
    file.read_to_string(&mut content)
        .map_err(|err| format!("Error reading file: {}", err))?;

    let yaml_data: YamlData =
        serde_yaml::from_str(&content).map_err(|err| format!("Error parsing YAML: {}", err))?;

    Ok(yaml_data)
}

pub fn extract_stage(yaml_data: &YamlData) -> Option<&str> {
    yaml_data.jobs.iter().find_map(|job| Some(&job.stage)).map(|x| x.as_str())
}

pub fn fill_markdown_content(yaml_data: &YamlData) -> String {
    if let Some(stage) = extract_stage(yaml_data) {
        format!(
            "# Job Specification\n\n\
            ## Stage\n\
            {}\n",
            stage
        )
    } else {
        String::new()
    }
}
