# Politiques et procédures de sécurité

Ce document décrit les procédures de sécurité et les politiques générales pour ce projet initialisé par Jules Agostini et maintenu par lui-même et la communauté, mentionné équipe de développement ci-dessous.

- [Politiques et procédures de sécurité](#politiques-et-procédures-de-sécurité)
  - [Signaler une faille](#signaler-une-faille)
    - [Possibilité 1 : mail](#possibilité-1--mail)
    - [Possibilité 2 : issues](#possibilité-2--issues)
    - [Criticité élevé](#criticité-élevé)
  - [Politique de divulgation et de correction](#politique-de-divulgation-et-de-correction)
  - [Commentaires sur cette politique](#commentaires-sur-cette-politique)

## Signaler une faille

L'équipe de développement prends très aux sérieux la sécurité de l'application.

### Possibilité 1 : mail

Signalez toute faille de sécurité en envoyant un mail au responsable du projet : `abuse@jagostini.fr`

### Possibilité 2 : issues

Vous pouvez également utiliser un des modèles d'issues pour renseigner le maximum d'information.

### Criticité élevé

Si la faille vous paraît critique, vous pouvez utiliser la clé gpg suivante pour chiffrer votre alerte :

```gpg

TODO: A générer

```

ex: `gpg --encrypt --recipient-file "fichier_avec_la_clé_publique" --output "fichier_chiffré" "fichier_en_clair"`

L'équipe accusera réception de votre mail dans les 72 heures. Après la réponse initiale à votre rapport, elle vous tiendra informé de la progression vers une correction et une annonce complète, et pourra vous demander des informations ou des conseils supplémentaires.

## Politique de divulgation et de correction

Lorsque l'équipe reçoit un rapport sur une faille de sécurité, elle procède aux étapes suivantes :

- Confirmer le problème et déterminer les versions affectées.
- Vérifier le code pour trouver tout problème similaire potentiel.
- Communiquer aux différentes instances connues qu'une faille est en cours de résolution.
- Préparer les correctifs, les merger sur la branche de production et les déployer sur les instances géré par l'équipe de développement.
- Communiquer aux différentes instances connues que le correctif est disponible sur la branche principale et a été actualisé sur les instances géré par l'équipe de développement.

## Commentaires sur cette politique

Si vous avez des suggestions sur la façon dont ce processus pourrait être amélioré, veuillez soumettre une issue avec le label `suggestion`.
